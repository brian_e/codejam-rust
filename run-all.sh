#! /bin/bash

set -eu

cargo build --release
cargo test --release
cargo bench
cargo doc

for example in ./examples/*.rs; do
	./run.sh "$(basename ${example/.rs/})"
done
