#![feature(test)]
#![allow(unused_features)]
#![deny(warnings)]

use std::fmt::{Debug, Display};
use std::io;
use std::io::prelude::*;
use std::str::FromStr;

pub mod alienlanguage;

pub fn jam<T: Display>(solve: &Fn(&mut Iterator<Item = io::Result<String>>) -> T) {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();
    let case_count = parse::<i32>(lines.next());
    for case in 1..case_count + 1 {
        let solution = solve(&mut lines);
        println!("Case #{}: {}", case, solution);
    }
}

pub fn with_next_line(lines: &mut Iterator<Item = io::Result<String>>,
                      process_line: &mut FnMut(&String)) {
    process_line(&lines.next().unwrap().unwrap());
}

pub fn parse<T: FromStr>(line: Option<io::Result<String>>) -> T
    where T::Err: Debug
{
    line.unwrap().unwrap().parse::<T>().unwrap()
}
