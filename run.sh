#! /bin/bash

set -eu

example="${1}"

cargo build --release --example ${example}

for path in input/${example}-*; do
    echo "${path/input/examples/output}"
	RUST_BACKTRACE=1 time cargo run --release -q --example ${example} <${path} >"${path/input/examples/output}"
    if [[ "${path/input/examples/output}" == "examples/output/${example}-sample.txt" ]]; then
        cat "examples/output/${example}-sample.txt"
    fi
done
