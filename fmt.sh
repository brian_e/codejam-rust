#! /bin/bash

# set -eux

for f in src/*.rs; do
	rustfmt -v --write-mode=overwrite ${f}
done

for f in examples/*.rs; do
	rustfmt -v --write-mode=overwrite ${f}
done
