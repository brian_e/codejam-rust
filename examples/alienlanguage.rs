#![deny(warnings)]

use std::io;
use std::io::prelude::*;

extern crate codejam;
use codejam::alienlanguage::Problem;

fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();
    let problem = Problem::from_lines(&mut lines);
    for case in problem {
        println!("{}", case);
    }
}
