#![deny(warnings)]

extern crate codejam;

use codejam::{jam, with_next_line};

fn main() {
    jam(&|lines| {
        let mut reverse_words = Vec::new();
        with_next_line(lines,
                       &mut |line| {
                           for word in line.split_whitespace().rev() {
                               reverse_words.push(word.to_string());
                           }
                       });
        return reverse_words.join(" ");
    });
}
