#![deny(warnings)]

extern crate codejam;

use codejam::{jam, with_next_line, parse};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
struct Item {
    price: i32,
    id: i32,
}

fn main() {
    jam(&|lines| {
        let credit = parse::<i32>(lines.next());
        let item_count = parse::<usize>(lines.next());
        let mut items = Vec::with_capacity(item_count);

        with_next_line(lines,
                       &mut |line| {

            for (i, price) in line.split_whitespace().enumerate() {
                items.push(Item {
                    price: price.parse::<i32>().unwrap(),
                    id: i as i32 + 1,
                });
            }
        });

        assert_eq!(item_count, items.len());
        items.sort();
        for (i, left) in items.iter().enumerate() {
            let goal = credit - left.price;
            let tail = &items[i + 1..];
            if let Ok(j) = tail.binary_search_by(|r| r.price.cmp(&goal)) {
                let mut ids = [left.id, tail[j].id];
                ids.sort();
                return format!("{} {}", ids[0], ids[1]);
            }
        }
        panic!();
    });
}
