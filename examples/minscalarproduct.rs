#![deny(warnings)]
extern crate codejam;
use codejam::{jam, with_next_line, parse};
fn main() {
    jam(&|lines| {
        let n = parse::<usize>(lines.next());
        let mut v1 = Vec::with_capacity(n);
        let mut v2 = Vec::with_capacity(n);
        for v in [&mut v1, &mut v2].iter_mut() {
            with_next_line(lines,
                           &mut |line| {
                               for coordinate in line.split_whitespace() {
                                   v.push(coordinate.parse::<i64>().unwrap());
                               }
                           });
            assert_eq!(n, v.len());
        }
        v1.sort();
        v2.sort_by(|a, b| b.cmp(a));
        let mut solution = 0;
        for (x, y) in v1.iter().zip(v2.iter()) {
            solution += x * y;
        }
        return solution;
    });
}
