#![deny(warnings)]

use std::io::BufReader;
use std::fs::File;
use std::io::prelude::*;

extern crate codejam;
use codejam::alienlanguage::Problem;

fn go(input_name: &str) {
    let expected_f = File::open(format!("tests/expected/alienlanguage-{}.txt", input_name)).unwrap();
    let mut expected_lines = BufReader::new(expected_f).lines();
    let mut next_expected_line = || expected_lines.next().unwrap().unwrap();
    let input_f = File::open(format!("input/alienlanguage-{}.txt", input_name)).unwrap();
    let mut lines = BufReader::new(input_f).lines();
    let problem = Problem::from_lines(&mut lines);
    for case in problem {
        assert_eq!(case, next_expected_line());
    }
}

#[test]
fn alienlanguage_sample() {
    go("sample");
}

#[test]
fn alienlanguage_small() {
    go("small");
}

#[test]
fn alienlanguage_large() {
    go("large");
}
