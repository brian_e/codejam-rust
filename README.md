# Google code jam past problems solved in Rust


## Store credit

* [Problem statement](https://code.google.com/codejam/contest/351101/dashboard#s=p0)
* [My solution](examples/storecredit.rs) 

Run it...

    $ ./run.sh storecredit


## Reverse words

* [Problem statement](https://code.google.com/codejam/contest/351101/dashboard#s=p1)
* [My solution](examples/reversewords.rs)

Run it...

    $ ./run.sh reversewords


## Minimum scalar product

* [Problem statement](https://code.google.com/codejam/contest/32016/dashboard#s=p0)
* [My solution](examples/minscalarproduct.rs)

Run it...

    $ ./run.sh minscalarproduct


## Alien language

* [Problem statement](https://code.google.com/codejam/contest/90101/dashboard#s=p0&a=0)
* My solution: [example](examples/alienlanguage.rs) and [module](src/alienlanguage.rs)

Run it...

    $ ./run.sh alienlanguage
